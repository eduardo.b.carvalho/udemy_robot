*** Settings ***
Documentation    Testes da tela do dashboard do App mobile da Jaé

*** Test Cases ***
Visualiar o saldo após clicar no ícone "olho"
    Dado que estou logado no app móvel e no dashboard
    Clicar no ícone "olho"
    Verificar que o saldo não estará mais oculto
    Clicar no ícone "olho"
    Verificar que o saldo não estará mais visível

Buscar pontos de atendimento
    Dado que estou logado no app móvel e no dashboard
    Clicar no bloco "Pontos de atendimento"
    Clicar no Bloco "Selecione"
    Selecionar     "Ponto de atendimento Centro"
    Então o sistema mostrará no mapa o endereço do ponto de atendimento no centro
    
Verificar ponto de atendimento no google maps
    Dado que estou logado no app móvel e no dashboard
    Clicar no bloco "Pontos de atendimento"
    Clicar no Bloco "Selecione"
    Selecionar     "Ponto de atendimento Centro"
    Clicar no botão "Abrir no Google Maps"
    Então o sistema mostrará no Google Maps o endereço do ponto de atendimento no centro

Registrar uma dúvida
    Dado que estou logado no app móvel e no dashboard
    Clicar no bloco "Dúvidas?"
    Então o sistema redirecionará para uma página com um modal do whatsapp que contem um assistente virtual

Verificar uma notícia no dashboard
    Dado que estou logado no app móvel e no dashboard
    Clicar no bloco "Se liga nessas notícias" ou nas imagens abaixo desse bloco
    Clicar em "Saiba mais" da notícia    "Vai de Jaé"
    O sistema colapsará o bloco "Saiba mais" e mostrará o texto da notícia

