*** Settings ***
Documentation    Testes da tela de login do App mobile da Jaé

*** Test Cases ***
Logar no sistema através de login e senha com sucesso
    Dado que já tenho um cadastro válido
    Abrir app do Jaé    
    Inserir um CPF válido e uma senha válida
    ...    e clicar no botão entrar
    Então o sistema me redirecionará para o dashboard

Logar no sistema através de biometria com sucesso
    Dado que já tenho um cadastro válido
    E que eu já tenha habilitado login usando biometria
    Abrir app do Jaé    
    Clicar na imagem de uma digital
    Inserir sua digital no leitor
    Então o sistema me redirecionará para o dashboard

Tentar logar no sistema usando um login inválido
    Dado que já tenho um cadastro válido
    Abrir app do Jaé    
    Inserir um CPF inválido e uma senha válida
    ...    e clicar no botão entrar
    Então o sistema enviará um pop-up com a mensagem "Ops... Usuário ou senha incorreta!"

Tentar logar no sistema usando uma senha inválida
    Dado que já tenho um cadastro válido
    Abrir app do Jaé    
    Inserir um CPF válido e uma senha inválida
    ...    e clicar no botão entrar
    Então o sistema enviará um pop-up com a mensagem "Ops... Usuário ou senha incorreta!"

Retirar ocultação do campo senha
    Abrir app do Jaé
    Digitar no campo senha a palavra    teste123!
    Clicar no ícone "olho"
    Validar que a senha digitada ficará visível
    Clicar no ícone "olho"
    Validar que a senha digitada ficará oculta

Sistema lembrará do último login utilizado
    Dado que já tenho um cadastro válido
    Abrir app do Jaé    
    Inserir um CPF válido e uma senha válida
    ...    e clicar no botão Lembrar acesso
    ...    e clicar no botão entrar
    O sistema me redirecionará para o dashboard
    Realizar o logout
    Então o sistema voltará para a tela de login e o campo login conterá o último login utilizado