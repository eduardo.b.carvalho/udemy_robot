*** Settings ***
Documentation    Page objects da tela account_details

Library    SeleniumLibrary

*** Variables ***


*** Keywords ***

Alterar detalhes da conta
    [Arguments]    ${first_name}    ${last_name}    ${display-name}
    #Clear Element Text    locator=id:account_display_name
    Input Text    id:account_first_name      ${first_name}      clear=${True}
    Input Text    id:account_last_name       ${last_name}       clear=${True}    
    Input Text    id:account_display_name    ${display-name}    clear=${True}
    Click Button    css:button[type="submit"][value="Save changes"]

Validar erro ao configurar os detalhes da conta
    ${texto_erro}    Get WebElement    class:woocommerce-error
    Log To Console    ${texto_erro.text}

    @{lista_opções}    Create List    First name is a required field.    Last name is a required field.    Display name is a required field.
    # Wait Until Element Contains    css:li[data-id="account_first_name"]      First name is a required field.
    # Wait Until Element Contains    css:li[data-id="account_last_name"]       Last name is a required field.
    # Wait Until Element Contains    css:li[data-id="account_display_name"]    Display name is a required field.
    Should Contain Any    ${texto_erro.text}    @{lista_opções}

