*** Settings ***
Documentation    Page objects da tela login

Library    SeleniumLibrary

*** Variables ***


*** Keywords ***

Realizar login
    [Arguments]    ${user}    ${pwd}
    Input Text        id:username    ${user}
    Input Password    id:password    ${pwd}
    Click Button    css:[id="customer_login"] button[name="login"]

    Wait Until Page Contains    From your account dashboard you can view your

Realizar login incorreto
    [Arguments]    ${user}    ${pwd}
    Input Text        id:username    ${user}
    Input Password    id:password    ${pwd}
    Click Button    css:[id="customer_login"] button[name="login"]

    Wait Until Element Is Visible    class:woocommerce-error
    Wait Until Element Contains    locator=class:woocommerce-error    text=Error

Realizar logout
    Click Element    css:[id="post-9"] li[class*="customer-logout"]
    #Click Link    css:[role='alert'] a[href='http://demostore.supersqa.com/my-account/customer-logout/?_wpnonce=5c0fd6672a']
    Wait Until Element Is Visible    id:username
    Wait Until Element Is Visible    id:password
