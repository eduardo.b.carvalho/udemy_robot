*** Settings ***
Documentation    Page objects da tela my-account

Library    SeleniumLibrary

*** Variables ***


*** Keywords ***

Acessar account details
    Wait Until Element Is Visible    css:[class*='MyAccount-navigation'] a[href='http://demostore.supersqa.com/my-account/edit-account/']
    Click Element    css:[class*='MyAccount-navigation'] a[href='http://demostore.supersqa.com/my-account/edit-account/']
    Wait Until Element Contains    locator=css:h1[class='entry-title']    text=Account details  


Validar detalhes da conta no dashboard
    [Arguments]    ${display-name}
    Wait Until Page Contains    ${display-name}
    ${texto_dashboard}=    Get WebElement    class:woocommerce-MyAccount-content
    Log To Console    ${texto_dashboard.text}  
    Should Contain    ${texto_dashboard.text}    ${display-name}
    

