*** Settings ***
Documentation    Page objects da tela account_details


Library    SeleniumLibrary

*** Variables ***


*** Keywords ***

Alterar endereço de entrega
    [Arguments]    ${rua}    ${cidade}    ${estado}    ${cep}    ${pais}    ${telefone}
    Click Element    css:a[href="http://demostore.supersqa.com/my-account/edit-address/billing/"]
    Wait Until Element Contains    css:h1[class="entry-title"]    Addresses
    #País
    Select From List By Label    css:select[id="billing_country"]    ${pais}
    # Click Element    id:select2-billing_country-container
    # Input Text       css:input[class="select2-search__field"]    ${pais}
    # Click Element    id:select2-billing_country-result-d6e7-BR
    
    #rua
    Input Text       id:billing_address_1    ${rua}
    
    #Cidade
    Input Text       id:billing_city    ${cidade}

    #Estado
    Select From List By Label    css:select[id="billing_state"]    ${estado}

    #CEP
    Input Text       id:billing_postcode    ${cep}

    #Telefone
    Input Text       id:billing_phone    ${telefone}
    
    Click Button    css:button[type="submit"][value="Save address"]

Validar endereço de entrega
    [Arguments]    ${rua}    ${cidade}    ${estado}    ${cep}    ${pais}
    Wait Until Element Is Visible   css:div[class="woocommerce-message"]
    ${texto_sucesso}    Get WebElement    css:div[class="woocommerce-message"]
    Should Contain    ${texto_sucesso.text}    Address changed successfully.

    ${endereco}    Get WebElement    css:div[class='u-column1 col-1 woocommerce-Address']
    Should Contain    ${endereco.text}    ${rua}
    Should Contain    ${endereco.text}    ${cidade}
    Should Contain    ${endereco.text}    ${estado}
    Should Contain    ${endereco.text}    ${cep}
    Should Contain    ${endereco.text}    ${pais}





