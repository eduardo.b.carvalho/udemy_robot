*** Settings ***
Documentation    Page objects da tela de checkout


Library    SeleniumLibrary
Library    String

*** Variables ***


*** Keywords ***
Realizar checkout
    [Arguments]    ${lista}
    
    Wait Until Element Is Visible    css:h1[class="entry-title"]
    FOR  ${i}  IN  @{lista}
        Element Should Contain    id:order_review    ${i}
    END

    Wait Until Element Is Visible    id:place_order
    Mouse Over       id:place_order
    Click Element    id:place_order

    Sleep    5s


    
