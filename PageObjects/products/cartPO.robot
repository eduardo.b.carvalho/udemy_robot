*** Settings ***
Documentation    Page objects da tela do carrinho


Library    SeleniumLibrary
Library    String

*** Variables ***


*** Keywords ***
Validar carrinho
    [Arguments]    ${prod}    ${qtd}    ${valor}
    ${table}    Get WebElement    css:table[class="shop_table shop_table_responsive cart woocommerce-cart-form__contents"]
    
    ${count}    Get Element Count    //table[@class="shop_table shop_table_responsive cart woocommerce-cart-form__contents"]/tbody/tr

    FOR  ${i}  IN RANGE  ${count}
        ${j}    Evaluate    ${i}+1
        ${row}    Get WebElement    //table[@class="shop_table shop_table_responsive cart woocommerce-cart-form__contents"]/tbody/tr[${j}]
        ${resposta}    Run Keyword And Return Status    Should contain    ${row.text}    ${prod}
        IF  '${resposta}' == 'True'
            ${tb_product}    Get WebElement    //table[@class="shop_table shop_table_responsive cart woocommerce-cart-form__contents"]/tbody/tr[${j}]/td[@class='product-name']
            ${tb_price}    Get WebElement    //table[@class="shop_table shop_table_responsive cart woocommerce-cart-form__contents"]/tbody/tr[${j}]/td[@class='product-price']
            ${tb_subtotal}    Get WebElement    //table[@class="shop_table shop_table_responsive cart woocommerce-cart-form__contents"]/tbody/tr[${j}]/td[@class='product-subtotal']

            Should Be Equal    ${tb_product.text}    ${prod}

            ${calculado}    Evaluate    ${qtd}*${valor}
            ${calculado}    Convert To Number    ${calculado}    precision=2

            ${tb_subtotal}    Strip String    ${tb_subtotal.text}    characters=$
            ${tb_subtotal}    Convert To Number    ${tb_subtotal}    precision=2

            Should Be Equal    ${tb_subtotal}    ${calculado}

        END
        
    END
    
Limpar carrinho
    ${resposta}    Run Keyword And Return Status    Wait Until Element Is Visible    css:table[class="shop_table shop_table_responsive cart woocommerce-cart-form__contents"]
    IF  '${resposta}' == 'True'
        ${table}    Get WebElement    css:table[class="shop_table shop_table_responsive cart woocommerce-cart-form__contents"]
        ${count}    Get Element Count    //table[@class="shop_table shop_table_responsive cart woocommerce-cart-form__contents"]/tbody/tr[${j}]
        FOR  ${i}  IN RANGE    ${count}
            
            Click Element    //*[@id="post-7"]//tr[1]/td[1]/a[@class='remove']
            Sleep    3s

            ${resposta}    Run Keyword And Return Status    Wait Until Element Is Visible    css:table[class="shop_table shop_table_responsive cart woocommerce-cart-form__contents"]
            Log To Console    ${resposta}
            Exit For Loop If    ${resposta} == 'False'
            
        END
        

    END
    Log To Console     Carrinho está limpo
    