*** Settings ***
Documentation    Page objects da tela de produto


Library    SeleniumLibrary

*** Variables ***


*** Keywords ***
Procurar produto
    [Arguments]    ${prod}
    Input Text    id:woocommerce-product-search-field-0    ${prod}
    Press Keys    id:woocommerce-product-search-field-0    ENTER


Selecionar produto
    [Arguments]    ${prod}    ${qtd}    ${preco}
    ${prod_text}    Get WebElement    css:h1[class="product_title entry-title"]
    
    Should Be Equal    ${prod_text.text}    ${prod}
    
    Input Text    css:input[name="quantity"]    ${qtd}
    Click Button    css:button[type="submit"][name="add-to-cart"]

    Wait Until Element Is Visible    class:woocommerce-message
    ${message}    Get WebElement    class:woocommerce-message
    Should Contain Any    ${message.text}    has been added to your cart    have been added to your cart






