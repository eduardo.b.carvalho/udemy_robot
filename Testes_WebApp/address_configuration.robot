*** Settings ***
Documentation    Testes de address de E-commerce
#    robot -d .\results -L trace .\Testes_WebApp\address_configuration.robot

Resource    ../PageObjects/my-accountPO.robot
Resource    ../PageObjects/addressPO.robot
Resource    ../resource/session_rsc.robot
Resource    ../data_driven/credenciais.robot

Suite Setup    Abrir navegador e fazer login    user=${login}    password=${password}    URL=${base_url}    URL_complementar=/my-account/    BROWSER=chrome
Suite Teardown    Logout e fechar navegador    URL=${base_url}    URL_complementar=/my-account/

*** Variables ***
${first_name}=      Eduardo
${last_name}=       Borges
${display_name}=    Edu

*** Test Cases ***
# Acessar a área account details
#     Acessar account details

Configurar endereço de entrega
    Navegar para    URL=${base_url}    URL_complementar=/my-account/edit-address/
    Alterar endereço de entrega    Teste 1234    BH    Minas Gerais    35060-970    Brazil    12345678
    Validar endereço de entrega    Teste 1234    BH    Minas Gerais    35060-970    Brazil
