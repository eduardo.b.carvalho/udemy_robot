*** Settings ***
Documentation    Testes de account details de E-commerce
#    robot -d .\results -L trace .\Testes_WebApp\account_details.robot

Resource    ../PageObjects/my-accountPO.robot
Resource    ../PageObjects/account_detailsPO.robot
Resource    ../resource/session_rsc.robot
Resource    ../data_driven/credenciais.robot

Suite Setup    Abrir navegador e fazer login    user=${login}    password=${password}    URL=${base_url}    URL_complementar=/my-account/    BROWSER=chrome
Suite Teardown    Logout e fechar navegador    URL=${base_url}    URL_complementar=/my-account/

*** Variables ***
${first_name}=      Eduardo
${last_name}=       Borges
${display_name}=    Edu

*** Test Cases ***
# Acessar a área account details
#     Acessar account details

Configurar detalhes da conta
    Acessar account details
    Alterar detalhes da conta    ${first_name}    ${last_name}    ${display_name}
    Validar detalhes da conta no dashboard    ${display_name}
    
Configurar incorretamente os detalhes da conta
    Acessar account details
    Alterar detalhes da conta    ${EMPTY}    ${EMPTY}    ${EMPTY}
    Validar erro ao configurar os detalhes da conta
    
