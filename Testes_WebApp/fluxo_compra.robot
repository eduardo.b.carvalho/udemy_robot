*** Settings ***
Documentation    Testes de fluxo de compra
#    robot -d .\results -L trace .\Testes_WebApp\fluxo_compra.robot

Resource    ../PageObjects/my-accountPO.robot
Resource    ../PageObjects/products/cartPO.robot
Resource    ../PageObjects/products/checkoutPO.robot
Resource    ../PageObjects/products/productPO.robot
Resource    ../resource/session_rsc.robot
Resource    ../data_driven/credenciais.robot

Suite Setup    Abrir navegador e fazer login    user=${login}    password=${password}    URL=${base_url}    URL_complementar=/my-account/    BROWSER=chrome
Suite Teardown    Logout e fechar navegador    URL=${base_url}    URL_complementar=/my-account/

*** Variables ***
@{lista_produto}=      Album    Beanie with Logo

*** Test Cases ***
Testar fluxo de compra
    Navegar para    URL=${base_url}    URL_complementar=/
    Procurar produto    Album
    Selecionar produto    Album    2    15

    Procurar produto    Beanie with Logo
    Selecionar produto    Beanie with Logo    2    18

    Navegar para    URL=${base_url}    URL_complementar=/cart/
    Validar carrinho    Album    2    15
    Validar carrinho    Beanie with Logo    2    18

    Navegar para    URL=${base_url}    URL_complementar=/checkout/
    Realizar checkout    ${lista_produto}

Limpar carrinho de compra
    Navegar para    URL=${base_url}    URL_complementar=/cart/
    Limpar carrinho
