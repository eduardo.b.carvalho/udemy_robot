*** Settings ***
Documentation    Testes de login de E-commerce
#    robot -d .\results -L trace .\Testes_WebApp\login_ecommerce.robot

Resource    ../PageObjects/loginPO.robot
Resource    ../resource/session_rsc.robot
Resource    ../data_driven/credenciais.robot

Suite Setup    Abrir navegador    URL=${base_url}
Test Setup    Navegar para    ${base_url}    /my-account/
Suite Teardown    Fechar navegador

*** Variables ***


*** Test Cases ***
Teste de login com sucesso
    Realizar login    ${login}    ${password}
    Realizar logout

Teste de login SEM sucesso
    Realizar login incorreto   ${login}    1234

