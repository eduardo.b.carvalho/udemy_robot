*** Settings ***
Documentation    Testes da tela de novo cadastro do App mobile da Jaé

*** Test Cases ***
Abertura de conta com sucesso
    Abrir app do Jaé
    Clicar no botão "Abra sua conta"
    Inserir um CPF válido 
    ...    e aceitar os termos de uso 
    ...    e clicar no botão continuar
    Inserir o nome completo e data de nascimento válido 
    ...    e clicar no botão continuar 
    Inserir um número de celular e email válido 
    ...    e clicar no botão continuar
    Aceitar o captcha
    Verificar que o código enviado por celular e email são idênticos
    Inserir o código
    Validar o acesso ao sistema e que o cadastro foi criado corretamente

### Tela do CPF
Abertura de conta com CPF inválido
    Abrir app do Jaé
    Clicar no botão "Abra sua conta"
    Inserir um CPF inválido 
    ...    e clicar no botão continuar
    Então o sistema enviará um pop-up com a mensagem "Ops... Por favor, inserir CPF válido"

Abertura de conta sem aceitar os termos de uso
    Abrir app do Jaé
    Clicar no botão "Abra sua conta"
    Inserir um CPF válido
    ...    e clicar no botão continuar
    Então o sistema enviará um pop-up com a mensagem "Ops... É necessário aceitar os termos de uso"

# Tela de nome e data de nascimento
Abertura de conta com nome inválido
    Abrir app do Jaé
    Clicar no botão "Abra sua conta"
    Inserir um CPF válido 
    ...    e aceitar os termos de uso 
    ...    e clicar no botão continuar
    Inserir um nome inválido
    #nome será inválido caso use numeral, caracteres especiais, apenas um caracter no primeiro nome ou não haja um sobrenome, identificado por um espaço e ao menos um caracter
    Então o sistema mostrará uma mensagem abaixo do campo nome "Por favor inserir nome e sobrenome"

Abertura de conta com data de nascimento inválida
    Abrir app do Jaé
    Clicar no botão "Abra sua conta"
    Inserir um CPF válido 
    ...    e aceitar os termos de uso 
    ...    e clicar no botão continuar
    Inserir um nome válido
    #nome será inválido caso use numeral, caracteres especiais, apenas um caracter no primeiro nome ou não haja um sobrenome, identificado por um espaço e ao menos um caracter
    Inserir uma data inválida
    ...    e clicar no botão continuar
    Então o sistema enviará um pop-up com a mensagem "Ops... Por favor inserir uma data de nascimento válida!"

# Tela de celular e email
Abertura de conta com número de celular inválido
    Abrir app do Jaé
    Clicar no botão "Abra sua conta"
    Inserir um CPF válido 
    ...    e aceitar os termos de uso 
    ...    e clicar no botão continuar
    Inserir o nome completo e data de nascimento válido 
    ...    e clicar no botão continuar 
    Inserir um número de celular inválido
    Inserir um email válido
    ...    e clicar no botão continuar
    Então o sistema enviará um pop-up com a mensagem "Ops... Por favor inserir telefone válido!"

Abertura de conta com email inválido
    Abrir app do Jaé
    Clicar no botão "Abra sua conta"
    Inserir um CPF válido 
    ...    e aceitar os termos de uso 
    ...    e clicar no botão continuar
    Inserir o nome completo e data de nascimento válido 
    ...    e clicar no botão continuar 
    Inserir um número de celular válido
    Inserir um email inválido
    ...    e clicar no botão continuar
    Então o sistema enviará um pop-up com a mensagem "Ops... Por favor inserir email válido!"


# Tela de código de segurança
Abertura de conta com código de segurança errado
    Abrir app do Jaé
    Clicar no botão "Abra sua conta"
    Inserir um CPF válido 
    ...    e aceitar os termos de uso 
    ...    e clicar no botão continuar
    Inserir o nome completo e data de nascimento válido 
    ...    e clicar no botão continuar 
    Inserir um número de celular e email válido 
    ...    e clicar no botão continuar
    Aceitar o captcha
    Verificar que o código enviado por celular e email são idênticos
    Inserir um código diferente do enviado para o email
    Então o sistema enviará um pop-up com a mensagem "Ops... Falha ao confirmar código!"

Reenviar codigo sms e email e validar que funcionam
    Abrir app do Jaé
    Clicar no botão "Abra sua conta"
    Inserir um CPF válido 
    ...    e aceitar os termos de uso 
    ...    e clicar no botão continuar
    Inserir o nome completo e data de nascimento válido 
    ...    e clicar no botão continuar 
    Inserir um número de celular e email válido 
    ...    e clicar no botão continuar
    Aceitar o captcha
    Verificar que o código enviado por celular e email são idênticos
    Clicar em reenviar o código
    Verificar que o código enviado por celular e email são idênticos
    Validar o acesso ao sistema e que o cadastro foi criado corretamente