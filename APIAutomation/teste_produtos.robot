***Settings***
Library    OperatingSystem
Library    Collections


Resource    ../Services/service_product.robot
Resource    ../Services/service.robot

Suite Setup    Criar sessão
Suite Teardown    Encerrar sessão

#    robot -d .\results -L trace .\APIAutomation\teste_produtos.robot
***Variables***
&{produto_1}    nome=Camiseta do Brasil    preco=100    quantidade=2    descricao=camiseta da selecao
&{produto_2}    nome=Camiseta da Argentina    preco=150    quantidade=3    descricao=camiseta da selecao
&{USER_ADM}    nome=teste QA    email=qa1@qa.com.br    password=teste    administrador=true
&{USER_ADM_FALSE}    nome=Segundo teste QA    email=qa2@qa.com.br    password=teste    administrador=false


***Test Cases***

# Teste 1 - Criação de produto
#     ${user_id}    Criação de usuario    ${USER_ADM}
#     ${token}      Realizar login    ${USER_ADM.email}    ${USER_ADM.password}    200
#     ${prod_id}    Criação de produto - primeira maneira    ${token}    produto_1    201
#     Leitura de produto    ${token}    ${prod_id}    200
#     Atualizar produto     ${token}    ${prod_id}    produto_2
#     Leitura de produto    ${token}    ${prod_id}    200
#     Deletar produto       ${token}    ${prod_id}    200
#     Deletar usuario       user_id=${user_id}        USER=${USER_ADM}

Teste 2 - Criação de produto
    ${user_id}    Criação de usuario    ${USER_ADM}
    ${token}      Realizar login    ${USER_ADM.email}    ${USER_ADM.password}    200
    ${prod_id}    Criação de produto - segunda maneira    ${token}    ${produto_1}    201
    Leitura de produto    ${token}    ${prod_id}    200
    Atualizar produto     ${token}    ${prod_id}    ${produto_2}
    Leitura de produto    ${token}    ${prod_id}    200
    Deletar produto       ${token}    ${prod_id}    200
    Deletar usuario       user_id=${user_id}    USER=${USER_ADM}

Teste 3 - Produto com USER sem permissão
    ${user_id_adm}    Criação de usuario    ${USER_ADM}
    ${user_id_false}    Criação de usuario    ${USER_ADM_FALSE}

    ${token_adm}      Realizar login    ${USER_ADM.email}    ${USER_ADM.password}    200

    ${prod_id}    Criação de produto - segunda maneira    ${token_adm}    ${produto_1}    201
    Leitura de produto    ${token_adm}    ${prod_id}    200

    ${token_no_adm}      Realizar login    ${USER_ADM_FALSE.email}    ${USER_ADM_FALSE.password}    200
    Deletar produto       ${token_no_adm}    ${prod_id}    403
        
    ${token_adm}      Realizar login    ${USER_ADM.email}    ${USER_ADM.password}    200
    Deletar produto       ${token_adm}    ${prod_id}    200
    
    Deletar usuario       user_id=${user_id_adm}      USER=${USER_ADM}
    Deletar usuario       user_id=${user_id_false}    USER=${USER_ADM_FALSE}



    
     