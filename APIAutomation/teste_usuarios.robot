***Settings***
Library    OperatingSystem
Library    Collections

Resource    ../ROBOT_BASIC/service.robot

Suite Setup    Criar sessão
Suite Teardown    Encerrar sessão

#    robot -d .\results -L trace .\APIAutomation\teste_usuarios.robot
***Variables***
&{USER_CRUD}    nome=teste QA    email=qa@qa.com.br    password=teste    administrador=true
&{USER_CRUD_2}    nome=Segundo teste QA    email=qa@qa.com.br    password=teste    administrador=true
***Test Cases***

CRUD de usuario
    ${user_id}    Criação de usuario        ${USER_CRUD}
    Leitura de usuario    user_id=${user_id}    expected_status=200    USER=${USER_CRUD}
    Atualizar usuario     user_id=${user_id}    USER=${USER_CRUD_2}
    Leitura de usuario    user_id=${user_id}    expected_status=200    USER=${USER_CRUD_2}
    Deletar usuario       user_id=${user_id}    USER=${USER_CRUD_2}
     

    