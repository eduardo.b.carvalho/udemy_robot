*** Settings ***
Documentation    modelo de documentação
#    robot -d .\results -L trace -i smoke_test .\ROBOT_BASIC\teste_unico.robot
#    robot -d .\results -L trace .\APIAutomation\teste_unico.robot
Library    RequestsLibrary

*** Variables ***
${email}=    testeqa@qa.com
${senha}=    teste
${base_url}    https://serverest.dev/ 

*** Test Cases ***
Teste rápido
    Criar sessão
    ${id}    Criar usuário    ${email}    ${senha}
    ${token}    Autenticar usuário    ${email}    ${senha}
    Deletar usuario    ${id}    200
    Encerrar sessão

*** Keywords ***

Criar sessão
    Create Session    alias=api    url=${base_url}

Criar usuário
    [Arguments]    ${email_cad}    ${senha_cad}
    ${header}    Create Dictionary    Content-Type=application/json
    ${resp}    POST On Session    alias=api    url=/usuarios    headers=${header}    data={"nome": "Fulano da Silva","email": "${email_cad}","password": "${senha_cad}","administrador": "true"}
    ${user_id}    Set Variable    ${resp.json()['_id']}
    Log To Console    ${user_id}
    RETURN    ${user_id}

Autenticar usuário
    [Arguments]     ${email_cad}    ${senha_cad}
    ${header}    Create Dictionary    Content-Type=application/json 
    ${resp}       POST On Session      alias=api    url=/login    headers=${header}    data={"email": "${email_cad}","password": "${senha_cad}"}
    ${token_adm}    Set Variable    ${resp.json()['authorization']}
    Log To Console    ${token_adm}
    RETURN    ${token_adm}

Deletar usuario
    [Arguments]    ${user_id}    ${expected_result}
    ${header}=     Create Dictionary    Content-Type=application/json
    DELETE On Session    alias=api    url=/usuarios/${user_id}    headers=${header}    expected_status=${expected_result}  
    
Encerrar sessão
    Delete All Sessions