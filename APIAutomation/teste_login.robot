*** Settings ***
Documentation    modelo de documentação
#    robot -d .\results -L trace .\APIAutomation\teste_login.robot
Resource    ../Services/service.robot

Suite Setup    Criar sessão
Suite Teardown    Encerrar sessão

*** Variables ***
${login}    qa4qa@qa.com.br
${senha}    teste
${senha_incorreta}    senhaErrada
&{user_CRUD}    nome=teste QA    email=${login}    password=${senha}    administrador=true

*** Test Cases ***
Autenticação com sucesso
    ${user_id}    Criação de usuario    ${user_CRUD}
    ${token}      Realizar login    ${login}    ${senha}    200
    Deletar usuario       user_id=${user_id}    USER=${user_CRUD}

Autenticação sem sucesso
    ${user_id}    Criação de usuario    ${user_CRUD}
    ${token}      Realizar login    ${login}    ${senha_incorreta}    401
    Deletar usuario       user_id=${user_id}    USER=${user_CRUD}


