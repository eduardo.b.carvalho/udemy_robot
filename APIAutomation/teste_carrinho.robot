***Settings***
Library    OperatingSystem
Library    Collections


Resource    ../Services/service_carrinho.robot
Resource    ../Services/service_product.robot
Resource    ../Services/service.robot

Suite Setup    Criar sessão
Suite Teardown    Encerrar sessão

#    robot -d .\results -L trace .\APIAutomation\teste_carrinho.robot
***Variables***
&{produto_1}    nome=Camiseta do Brasil    preco=100    quantidade=2    descricao=camiseta da selecao
&{produto_2}    nome=Camiseta da Argentina    preco=150    quantidade=3    descricao=camiseta da selecao
&{USER_ADM}    nome=teste QA    email=qa1@qa.com.br    password=teste    administrador=true
&{USER_ADM_FALSE}    nome=Segundo teste QA    email=qa2@qa.com.br    password=teste    administrador=true

***Test Cases***

Teste - Fluxo completo com 1 produto
    ${user_id_adm}    Criação de usuario    ${USER_ADM}
    ${token_adm}      Realizar login    ${USER_ADM.email}    ${USER_ADM.password}    200
    ${prod_id}    Criação de produto - segunda maneira    ${token_adm}    ${produto_1}    201
    
    ${carrinho_id}    Cadastrar produto no carrinho    ${prod_id}    ${produto_1}    ${token_adm}    201
    Leitura de carrinho    ${token_adm}    ${produto_1}    ${carrinho_id}    200

    Realizar compra    ${token_adm}    200
    Leitura de produto        ${token_adm}    ${prod_id}    200

    Excluir carrinho / Cancelar compra    ${token_adm}    200
    Leitura de produto        ${token_adm}    ${prod_id}    200
  
    Deletar produto       ${token_adm}    ${prod_id}    200
    Deletar usuario       user_id=${user_id_adm}      USER=${USER_ADM}
    



    
     