*** Settings ***
Documentation    Esse arquivo irá controlar as keywords de sessão

Resource    ../PageObjects/loginPO.robot
Library    SeleniumLibrary

*** Variables ***


*** Keywords ***

Abrir navegador
    [Arguments]    ${URL}    ${BROWSER}=chrome
    Open Browser  url=${URL}    browser=${BROWSER}    options=add_experimental_option("detach", True)

Fechar navegador
    Close Browser

Navegar para
    [Arguments]    ${URL}    ${URL_complementar}
    Go To    ${URL}${URL_complementar}

Abrir navegador e fazer login
    [Arguments]    ${user}    ${password}    ${URL}    ${URL_complementar}    ${BROWSER}
    Abrir navegador    ${URL}     ${BROWSER}
    Navegar para       ${URL}     ${URL_complementar}
    Realizar login     ${user}    ${password}

Logout e fechar navegador
    [Arguments]    ${URL}    ${URL_complementar}
    Navegar para    ${URL}    ${URL_complementar}
    Realizar logout
    Close Browser

