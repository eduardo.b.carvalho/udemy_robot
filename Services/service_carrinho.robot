*** Settings ***
Documentation    modelo de documentação
#    robot -d .\results -L trace .\ROBOT_BASIC\service.robot
Library    RequestsLibrary
Library    OperatingSystem
Library    String

*** Variables ***


*** Keywords ***

Cadastrar produto no carrinho
    [Arguments]    ${prod_id}    ${produto}    ${token}    ${expected_status}
    
    ${body}    Get File    path=${EXECDIR}/Fixtures/carrinho_template.json
    ${body}    Replace String Using Regexp    ${body}    _produto_id      ${prod_id}
    ${body}    Replace String Using Regexp    ${body}    "_quantidade"    ${produto}[quantidade]

    log to console    ${body}

    ${header}     Create Dictionary    Content-Type=application/json    Authorization=${token}  
    ${resp}       POST On Session      alias=api    url=/carrinhos    headers=${header}    data=${body}    expected_status=${expected_status}
    ${carrinho_id}    Set Variable    ${resp.json()['_id']}
    
    Log To Console    ${carrinho_id}
    [Return]    ${carrinho_id}


Leitura de carrinho
    [Arguments]    ${token}    ${produto}    ${carrinho_id}    ${expected_status}
    ${header}=     Create Dictionary    Content-Type=application/json    Authorization=${token}
    ${resp}        GET On Session    alias=api    url=/carrinhos/${carrinho_id}    headers=${header}    expected_status=${expected_status}
    Log To Console    ${resp.json()}

    IF  ${expected_status} == 200
        ${preco}    Set Variable    ${produto}[preco]
        ${preco_total}    Set Variable    ${resp.json()['precoTotal']}
        ${qtd}    Set Variable    ${resp.json()['quantidadeTotal']}

        ${preco}    Convert To Number    ${preco}
        ${preco_total}    Convert To Number    ${preco_total}
        ${qtd}    Convert To Number    ${qtd}

        ${preco_total_calculado}    Evaluate    ${preco}*${qtd}

        Should Be Equal    ${preco_total}    ${preco_total_calculado}  
    END
    

Realizar compra
    [Arguments]    ${token}    ${expected_status}

    ${header}=     Create Dictionary    Content-Type=application/json    Authorization=${token}
    ${resp}    DELETE On Session    alias=api    url=/carrinhos/concluir-compra/    headers=${header}    expected_status=${expected_status}
    Log To Console     ${resp.json()}

Excluir carrinho / Cancelar compra
    [Arguments]    ${token}    ${expected_status}

    ${header}=     Create Dictionary    Content-Type=application/json    Authorization=${token}
    ${resp}    DELETE On Session    alias=api    url=/carrinhos/cancelar-compra/    headers=${header}    expected_status=${expected_status}
    Log To Console     ${resp.json()}

