*** Settings ***
Documentation    modelo de documentação
#    robot -d .\results -L info -i smoke_test .\ROBOT_BASIC\setup_teardown.robot
#    robot -d .\results -L debug -i smoke_test .\ROBOT_BASIC\setup_teardown.robot
#    robot -d .\results -L trace -i smoke_test .\ROBOT_BASIC\setup_teardown.robot
Suite setup       Abrir browser
Suite teardown    Fechar browser
Test setup        Fazer login
Test teardown     Fazer logout


*** Variables ***


*** Test Cases ***
Test 1
    [Tags]    smoke_test
    [Documentation]    Doc do test 1
    Log To Console    Hello World 1
    

Test 2
    [Tags]    smoke_test
    Log To Console    Hello World 2
   

Test 3
    Log To Console    Hello World 2
    

*** Keywords ***

Abrir browser
    Log To Console    Abrindo o browser
Fechar browser
    Log To Console    Fechando o browser
Fazer login
    Log To Console    Fazendo o login
Fazer logout
    Log To Console    Fazendo o logout