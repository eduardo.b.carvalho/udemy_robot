*** Settings ***
Documentation    Keywords para a camada de serviços que atenderá sessões e as APIs de usuário
#    robot -d .\results -L trace .\ROBOT_BASIC\service.robot
Library    RequestsLibrary
Library    String
Library    OperatingSystem

*** Variables ***
${base_url}=   https://serverest.dev/ 


*** Keywords ***
Criar sessão
    Create Session       alias=api    url=${base_url}

Criação de usuario
    [Arguments]   ${USER_CRUD}
    ${body}    Get File    path=${EXECDIR}/Fixtures/usuario_template.json
    ${body}    Replace String Using Regexp    ${body}    _nome     ${USER_CRUD}[nome]
    ${body}    Replace String Using Regexp    ${body}    _email    ${USER_CRUD}[email]
    ${body}    Replace String Using Regexp    ${body}    _pwd      ${USER_CRUD}[password]
    ${body}    Replace String Using Regexp    ${body}    _admin    ${USER_CRUD}[administrador]

    ${header}     Create Dictionary    Content-Type=application/json
    ${resp}        POST On Session      alias=api    url=/usuarios    headers=${header}   data=${body}    expected_status=201
    ${user_id}    Set Variable    ${resp.json()['_id']}
    
    Log To Console    ${user_id}
    [Return]    ${user_id}

Leitura de usuario
    [Arguments]    ${user_id}    ${expected_status}    ${USER} 
    ${header}=     Create Dictionary    Content-Type=application/json
    ${resp}        GET On Session    alias=api    url=/usuarios/${user_id}    headers=${header}    expected_status=${expected_status}
    ${resp_get}    Set Variable    ${resp.json()}
    IF  '${expected_status}' == '200'
        ${email}    Set Variable    ${resp.json()['email']}
        Should Be Equal    ${email}    ${USER.email}
    END
    Log To Console    ${resp_get}

Atualizar usuario
    [Arguments]    ${user_id}    ${USER}
    ${body}    Get File    path=${EXECDIR}/Fixtures/usuario_template.json
    ${body}    Replace String Using Regexp    ${body}    _nome     ${USER}[nome]
    ${body}    Replace String Using Regexp    ${body}    _email    ${USER}[email]
    ${body}    Replace String Using Regexp    ${body}    _pwd      ${USER}[password]
    ${body}    Replace String Using Regexp    ${body}    _admin    ${USER}[administrador]

    ${header}=     Create Dictionary    Content-Type=application/json

    ${resp}        PUT On Session    alias=api    url=/usuarios/${user_id}    headers=${header}    data=${body}    expected_status=200
    ${resp_put}    Set Variable    ${resp.json()}
    Log To Console    ${resp_put}

Deletar usuario
    [Arguments]    ${user_id}    ${USER}
    ${header}=     Create Dictionary    Content-Type=application/json
    DELETE On Session    alias=api    url=/usuarios/${user_id}    headers=${header}    expected_status=200  
    ${resp}    Leitura de usuario    user_id=${user_id}    expected_status=400    USER=${USER}


Encerrar sessão
    Delete All Sessions

Realizar login
    [Arguments]    ${login}    ${senha}    ${expected_result}
    ${header}     Create Dictionary    Content-Type=application/json
    ${resp}       POST On Session      alias=api    url=/login    headers=${header}    data={"email": "${login}","password": "${senha}"}    expected_status=${expected_result}
    IF  ${expected_result}==401
        Should Be Equal    ${resp.json()['message']}    Email e/ou senha inválidos
        RETURN
    ELSE IF    ${expected_result}==200
        ${token}    Set Variable    ${resp.json()['authorization']}
        Log To Console    ${token}
        RETURN    ${token}
    END
    

      

