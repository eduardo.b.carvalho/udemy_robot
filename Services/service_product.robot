*** Settings ***
Documentation    modelo de documentação
#    robot -d .\results -L trace .\ROBOT_BASIC\service.robot
Library    RequestsLibrary
Library    OperatingSystem
Library    String
Library    JSONLibrary

*** Variables ***


*** Keywords ***

Criação de produto - primeira maneira  
    [Arguments]   ${token}    ${file}    ${expected_status}
    #Getting the file
    IF  "${file}" == "produto_1"
        ${body}    Get File   path=${EXECDIR}/Fixtures/produto_1.json
        Log To Console    ${body}
    ELSE IF    "${file}" == "produto_2"
        ${body}    Get File   path=${EXECDIR}/Fixtures/produto_2.json
        Log To Console    ${body}
    END
    
    ${header}     Create Dictionary    Content-Type=application/json    Authorization=${token}  
    ${resp}       POST On Session      alias=api    url=/produtos    headers=${header}    data=${body}    expected_status=${expected_status}
    ${prod_id}    Set Variable    ${resp.json()['_id']}
    
    Log To Console    ${prod_id}
    [Return]    ${prod_id}

Criação de produto - segunda maneira  
    [Arguments]   ${token}    ${file}    ${expected_status}
    #Getting the file
    ${body}    Get File    path=${EXECDIR}/Fixtures/produto_template.json
    ${body}    Replace String Using Regexp    ${body}    _nome    ${file}[nome]
    ${body}    Replace String Using Regexp    ${body}    "_preco"    ${file}[preco]
    ${body}    Replace String Using Regexp    ${body}    _desc    ${file}[descricao]
    ${body}    Replace String Using Regexp    ${body}    "_quantidade"    ${file}[quantidade]

    Log To Console    ${body}
    
    ${header}     Create Dictionary    Content-Type=application/json    Authorization=${token}  
    ${resp}       POST On Session      alias=api    url=/produtos    headers=${header}    data=${body}    expected_status=${expected_status}
    ${prod_id}    Set Variable    ${resp.json()['_id']}
    
    Log To Console    ${prod_id}
    [Return]    ${prod_id}


Leitura de produto
    [Arguments]    ${token}    ${prod_id}    ${expected_status}
    ${header}=     Create Dictionary    Content-Type=application/json    Authorization=${token}
    ${resp}        GET On Session    alias=api    url=/produtos/${prod_id}    headers=${header}    expected_status=${expected_status}
    Log To Console    ${resp.json()}

Atualizar produto
    [Arguments]    ${token}    ${prod_id}    ${file}
    ${header}=     Create Dictionary    Content-Type=application/json    Authorization=${token}
    ${body}    Get File    path=${EXECDIR}/Fixtures/produto_template.json
    ${body}    Replace String Using Regexp    ${body}    _nome    ${file}[nome]
    ${body}    Replace String Using Regexp    ${body}    "_preco"    ${file}[preco]
    ${body}    Replace String Using Regexp    ${body}    _desc    ${file}[descricao]
    ${body}    Replace String Using Regexp    ${body}    "_quantidade"    ${file}[quantidade]
    
    ${resp}        PUT On Session    alias=api    url=/produtos/${prod_id}    headers=${header}    data=${body}    expected_status=200
    ${resp_put}    Set Variable    ${resp.json()}
    Log To Console    ${resp_put}

Deletar produto
    [Arguments]    ${token}    ${prod_id}    ${expected_status}
    ${header}=     Create Dictionary    Content-Type=application/json    Authorization=${token}
    ${resp}    DELETE On Session    alias=api    url=/produtos/${prod_id}    headers=${header}    expected_status=${expected_status}  
    Log To Console    ${resp.json()}
    IF  ${expected_status} == 200
        Leitura de produto    ${token}    ${prod_id}    400
    END
    
    


    

      

